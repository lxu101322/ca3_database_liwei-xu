import org.apache.log4j.Logger;

public class log4j_Test {
   
static Logger log = Logger.getLogger(log4j_Test.class.getName());

 public static void main(String[] args) {
    log.trace("This is a Trace");
    log.debug("This is a Debug");
    log.info("This is an Info");
    log.warn("This is a Warn");
    log.error("This is an Error");
    log.fatal("This is a Fatal");
 }
}
