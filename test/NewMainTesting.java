
import DAO.Dao;
import Exceptions.DaoException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NewMainTesting {

    public static void main(String[] args) throws DaoException, SQLException {
            MemberDAOJUnitTest a = new MemberDAOJUnitTest();
            
            Dao dao = new Dao();

            Connection conn = dao.getConnection();

            String s1 = "{CALL ADD_SERVICE(?,?)}";
           
            CallableStatement cs = conn.prepareCall(s1);

            cs.setInt(1, 0);
            cs.setString(2, "@answer");

            cs.execute();

            ResultSet rs = cs.getResultSet();

            rs.next();

            System.out.println("The first post by Id is "+rs.getString("flightNumber"));
            
    }
    
}
