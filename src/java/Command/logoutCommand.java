package Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class logoutCommand implements Command {
    public String execute(HttpServletRequest request,
        HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.invalidate();

        String forwardToJsp = "/index.jsp";
        return forwardToJsp;
    }
}
