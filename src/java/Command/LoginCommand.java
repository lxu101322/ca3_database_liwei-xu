
package Command;

import SYSTEM_Object.Admin;
import SYSTEM_Object.Member;
import Servis.Adminservice;
import Servis.Userservice;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
                        String name = request.getParameter( "username" );
                        String password = request.getParameter( "password" );
                        String forwardToJsp = null;
                       
                        HttpSession session  = request.getSession();
                        
                        Member user1=null;
                        Admin admin=null;
                        
                        Userservice userService = new Userservice();
                        user1 = userService.login(name, password);
                        
                        Adminservice adminservice = new Adminservice();
                        admin = adminservice.login(name, password);
                        
                     
                        
                        if(admin!=null){
                            session.setAttribute("us",null);
                            session.setAttribute("ad",admin);
                            forwardToJsp = "/home.jsp";
                        }else if(user1!=null){
                            session.setAttribute("listss",null);
                            session.setAttribute("arcc",null);
                            session.setAttribute("ad",null);
                            session.setAttribute("us",user1);
                            forwardToJsp = "/home.jsp";
                            ArrayList<String> arc = new ArrayList<String>();
                            arc = userService.listAll();
                            session.setAttribute("list", arc);
                            session.setAttribute("check","false");
                        }else{     
                            session.setAttribute( "Error", "Password or username is incorrent!"); 
                            forwardToJsp = "/index.jsp";
                        }
 
                        return  forwardToJsp;
    }
    
}
