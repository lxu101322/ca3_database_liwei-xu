
package Command;

import SYSTEM_Object.Member;
import Servis.Userservice;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class listAllUserCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
                     
                        String forwardToJsp = null;
                        HttpSession session  = request.getSession();
                        
                        Userservice userService = new Userservice();
                        List<Member> arc = null;
                        arc = userService.listAllUser();
                       session.setAttribute("check","false");
                        session.setAttribute("listss", arc);
                        forwardToJsp = "/home.jsp";
                        
                       
                        return  forwardToJsp;
    }
    
}
