package Servis;

import DAO.memberDao;
import DAO.ssDao;
import Exceptions.DaoException;
import SYSTEM_Object.Member;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Userservice {
    public Connection getConnection() throws DaoException {

        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/infosystemsql";
        String username = "root";
        String password = "";
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ex1) {
            System.out.println("Failed to find driver class " + ex1.getMessage());
            System.exit(1);
        } catch (SQLException ex2) {
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2);
        }
        return con;
    }
    
     public Member login(String username, String password) throws SQLException
    {
        memberDao dao = new memberDao();
        Member u = null;
        try 
        {
            u = dao.login(username, password);
            
            Connection con = null;  
        
        
        con = getConnection();
        String s1 = "{CALL memberAction(?)}";
    	CallableStatement cs = con.prepareCall(s1);
    	cs.setString(1, "member logged");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
     
    public Member register(String username, String password) throws SQLException
    {
        memberDao dao = new memberDao();
        int in;
        Member u = null;
        try 
        {
            in = dao.register(username, password);
            u = dao.login(username, password);
            
             Connection con = null;  
        
        
        con = getConnection();
        String s1 = "{CALL memberAction(?)}";
    	CallableStatement cs = con.prepareCall(s1);
    	cs.setString(1, "member resgister");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
    
    public int postService(String text, Member x) throws SQLException
    {
        ssDao dao = new ssDao();
        
        try 
        {
           dao.addsss(text, x);
             Connection con = null;  
        
        
        con = getConnection();
        String s1 = "{CALL memberAction(?)}";
    	CallableStatement cs = con.prepareCall(s1);
    	cs.setString(1, "member post");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return 1;
        
    }
    
    public ArrayList<String> listAll()
    {
        ssDao dao = new ssDao();
         ArrayList<String> arc = new ArrayList<String>();
        
        try 
        {
            arc = dao.listAll();
            
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return arc;
    }
    
    public List<Member> listAllUser()
    { 
        memberDao dao = new memberDao();
         List<Member> arc = null;
        try 
        {
        
         arc = dao.findAllUsers();
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return arc;
    }
    
    public int deleteUser(String username)
    { 
        ssDao dao = new ssDao();
        int a = 0;
        try 
        {
            a  = dao.delete(username);
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return a;
    }
    
}
