package DAO;

import DAOInterface.memberDaoInterface;
import Exceptions.DaoException;
import SYSTEM_Object.Member;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class memberDao extends Dao implements memberDaoInterface{
     
    public Member login(String MemberUsername,String MemberPassword) throws DaoException{
            Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            Member a = null;
       
            try {

                con = getConnection();         
                String query = "SELECT * FROM MEMBER WHERE username = ? AND password = ?";      
                ps = con.prepareStatement(query);
                ps.setString(1,MemberUsername);
                ps.setString(2,MemberPassword);
                rs = ps.executeQuery();
             
                if(rs.next()){
                    int id = rs.getInt("user_id");
                    String username = rs.getString("username");
                    String password = rs.getString("password");
                    a = new Member(id,username,password);
               }
        } catch (SQLException e) {
            throw new DaoException("login(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("login(): " + e.getMessage());
            }
        }          
        return a;
    }
    
    public int register(String MemberUsername,String MemberPassword) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        try {
            con = getConnection();
           
            String query = "SELECT username FROM member WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, MemberUsername);

            rs = ps.executeQuery();
            if (rs.next()) {
                throw new DaoException("Member Username " + MemberUsername + " already exists");
            }
            else{
            String command = "INSERT INTO member (username,password) VALUES(?, ?)";
            ps = con.prepareStatement(command);
            ps.setString(1, MemberUsername);
            ps.setString(2, MemberPassword);
       
            ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("register: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("register(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }
    
    public List<Member> findAllUsers() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Member> member = new ArrayList<Member>();

        try {
            con = this.getConnection();

            String query = "SELECT * FROM member";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("user_id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                Member u = new Member(id, username, password);
                member.add(u);
            }
        } catch (SQLException e) {
            throw new DaoException("findAllUsers() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException(e.getMessage());
            }
        }

        return member; 
    }
    
    
    
}
