package DAO;

import DAOInterface.adminDaoInterface;
import Exceptions.DaoException;
import SYSTEM_Object.Admin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class adminDao extends Dao implements adminDaoInterface{
    public Admin login(String AdminUsername,String AdminPassword) throws DaoException{
            Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            Admin a = null;
       
            try {

                con = getConnection();         
                String query = "SELECT * FROM infosystemsql.admin WHERE adminname = ?  AND adminpassword = ?";      
                ps = con.prepareStatement(query);
                ps.setString(1,AdminUsername);
                ps.setString(2,AdminPassword);
                rs = ps.executeQuery();
             
                if(rs.next()){
                  
                    String username = rs.getString("adminname");
                 
                    String password = rs.getString("adminpassword");
                 
                    a = new Admin(username,password);
                   
               }
        } catch (SQLException e) {
            throw new DaoException("login(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("login(): " + e.getMessage());
            }
        }          
        return a;
    
    }
    
    public int register(String AdminUsername,String AdminPassword) throws DaoException{
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        try {
            con = getConnection();
           
            String query = "SELECT username FROM admin WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, AdminUsername);

            rs = ps.executeQuery();
            if (rs.next()) {
                throw new DaoException("Admin Username " + AdminUsername + " already exists");
            }

            String command = "INSERT INTO admin (username,password) VALUES(?, ?)";
            ps = con.prepareStatement(command);
            ps.setString(1, AdminUsername);
            ps.setString(2, AdminPassword);
       
            rowsAffected = ps.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("register: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("register(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }  
}
