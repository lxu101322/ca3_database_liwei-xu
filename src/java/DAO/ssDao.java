
package DAO;

import Exceptions.DaoException;
import SYSTEM_Object.Member;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ssDao extends Dao {
    public void addsss(String ccc, Member x) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        try {
            con = getConnection();
           
            String query = "INSERT INTO status (ccc) VALUES(?)";
            ps = con.prepareStatement(query);
            ps.setString(1, ccc);
            ps.executeUpdate();
            
         
        } catch (SQLException e) {
            throw new DaoException("register: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("register(): " + e.getMessage());
            }
        }
   
    }
    
    public ArrayList<String> listAll() throws DaoException{
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
         ArrayList<String> arc = new ArrayList<String>();
        int rowsAffected = 0;
        try {
            con = getConnection();
           
            String query = "select * from status;";
            ps = con.prepareStatement(query);

            
            rs = ps.executeQuery();
             
                while(rs.next()) {
                  
                    String username = rs.getString("ccc");
                 
                  
                arc.add(username);
            }
            
         
        } catch (SQLException e) {
            throw new DaoException("register: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("register(): " + e.getMessage());
            }
        }
        return arc;
    }
    
    public int delete(String MemberUsername) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        try {
            con = getConnection();
 
            String command = "DELETE FROM member WHERE username = ?";
            ps = con.prepareStatement(command);
            ps.setString(1, MemberUsername);
            ps.executeUpdate();
            rowsAffected = 1;
        } catch (SQLException e) {
            throw new DaoException("delete: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("delete(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }
}
