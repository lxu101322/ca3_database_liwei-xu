

-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 04 月 03 日 00:20
-- 服务器版本: 5.5.24-log
-- PHP 版本: 5.3.13

CREATE DATABASE IF NOT EXISTS infosystemsql;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
use infosystemsql;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `infosystemsql`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(20) NOT NULL AUTO_INCREMENT,
  `adminname` varchar(20) NOT NULL,
  `adminpassword` varchar(20) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`admin_id`, `adminname`, `adminpassword`) VALUES
(1, 'LEE', 'aaa'),
(2, 'eoos', '123456'),
(3, 'poop', '225'),
(4, 'ppdod', 's666'),
(5, 'llodjd', '3355');

-- --------------------------------------------------------

--
-- 表的结构 `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `member_id` int(255) NOT NULL AUTO_INCREMENT,
  `member_firstname` varchar(15) NOT NULL,
  `member_secname` varchar(15) NOT NULL,
  `member_email` varchar(50) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

CREATE TABLE IF NOT EXISTS `memberTableAction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `actions` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `member`
--

INSERT INTO `member` (`user_id`, `username`, `password`) VALUES
(1, 'LEE', '122'),
(3, 'lol', 'pp'),
(4, 'ewwg', '334'),
(7, 'kkfnn', '99844'),
(8, 'LKKSJ', '2258'),
(9, 'OOPPPS', '3394'),
(10, 'LALSPOSW', 'SA3232'),
(11, 'DSVS', '3RFEFE'),
(12, 'PPLLLSSS', '1122'),
(14, 'kkdddd', '123');

-- --------------------------------------------------------

--
-- 表的结构 `privatemsg`
--

CREATE TABLE IF NOT EXISTS `privatemsg` (
  `privateMsg_id` int(255) NOT NULL AUTO_INCREMENT,
  `privateMsg_membername` varchar(15) NOT NULL,
  `privateMsg_time` date NOT NULL,
  `privateMsg_content` varchar(255) NOT NULL,
  `privateMsg_Memberid` int(255) NOT NULL,
  PRIMARY KEY (`privateMsg_id`),
  KEY `privateMsg_Memberid` (`privateMsg_Memberid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `sst_id` int(20) NOT NULL AUTO_INCREMENT,
  `ccc` varchar(100) NOT NULL,
  `Member_id` int(255) NOT NULL,
  `Member_name` varchar(50) NOT NULL,
  PRIMARY KEY (`sst_id`),
  KEY `Member_id` (`Member_id`),
  KEY `Member_id_2` (`Member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- 转存表中的数据 `status`
--

INSERT INTO `status` (`sst_id`, `ccc`, `Member_id`, `Member_name`) VALUES
(1, 'Hi, still in the office?', 0, ''),
(2, 'Hi, you must be Mary? ', 0, ''),
(3, 'Hi, glad to see you here. May I ask your name? ', 0, ''),
(4, 'Say “Hi” and smile a lot, with no expectations.  ', 0, ''),
(5, 'When we walked into the lobby, the nurse said, “Hi, Sean!” and then buzzed us in.', 0, ''),
(6, 'Hi, Paul! Are you from London? ', 0, ''),
(7, 'Hi, do you do delivering?', 0, ''),
(8, 'Hi Sis, What a day it has been today. ', 0, ''),
(9, 'Yes, I know. I''d studied it up in a book. ', 0, ''),
(10, 'Yes, indeed, I intend to go. ', 0, ''),
(11, 'Will everything be finished by tomorrow? —Yes,I believe so.  ', 0, ''),
(12, 'Not that it matters, but yes.', 0, ''),
(13, 'Yes, what flights do you have? ', 0, ''),
(14, 'Ken refused on account of explaining what happened would affect his image.', 0, ''),
(15, 'Ken''s mother won''t let him go out and play. ', 0, ''),
(16, 'Do you doubt that I love you?', 0, ''),
(17, 'Her letter hints of her love to (towards) me.  ', 0, ''),
(18, 'We should parent children with both love and discipline.', 0, ''),
(19, 'I love you because of what you do for me. ', 0, ''),
(20, 'Why do I love them?', 0, ''),
(21, 'They love the girl than(they love) him.', 0, ''),
(22, 'The pupils love their teacher.', 0, ''),
(23, 'She desired the love of the handsome youth.', 0, ''),
(24, 'The blind have a keen touch. ', 0, ''),
(25, 'Touch his phone or computer and he flips out.', 0, ''),
(26, 'Find reasons to keep in touch with the best of them. ', 0, ''),
(27, 'But I do have to touch base with the head of our loan department for his okay.', 0, ''),
(28, 'What can I touch? ', 0, ''),
(29, 'He is now out of touch with his old friends.', 0, ''),
(31, 'hgyuiuyggigii', 0, ''),
(32, '\r\n', 0, ''),
(33, '  FWFWFEWFW\r\n', 0, ''),
(34, '  \r\nFWEFWEFWE', 0, ''),
(35, '  32F23F23F3', 0, ''),
(36, '  \r\nSAY WHATEVER YOU WANT!', 0, ''),
(37, 'FFFEEEEEEEEEEEEEE', 0, ''),
(38, 'say whatever you want here...\r\n', 0, ''),
(39, '  dddddddddddd\r\n', 0, ''),
(40, '  \r\ndddddddddddddddd', 0, ''),
(41, '  \r\ndd', 0, ''),
(42, '  \r\n', 0, ''),
(43, '  \r\n', 0, ''),
(44, '  \r\n', 0, ''),
(45, '  \r\ndwdwd', 0, ''),
(46, '  \r\n', 0, ''),
(47, '  qwqwqqqqqqqqqqqq', 0, ''),
(48, '  qwqwqqqqqqqqqqqq', 0, ''),
(49, '  qwqwqqqqqqqqqqqq', 0, ''),
(50, '  \r\nwwwww', 0, ''),
(51, '  fewfwefwe', 0, ''),
(52, '  eeeeeeeeeeeeeeeeee\r\n', 0, ''),
(53, '  eeeeeeeeeeeeeeeeee\r\n', 0, ''),
(54, '  eeeeeeeeeeeeeeeeee\r\n', 0, ''),
(55, '  fewfwefwe', 0, ''),
(56, '  \r\nwwwww', 0, ''),
(57, '  sss\r\n', 0, ''),
(58, '  sss\r\n', 0, ''),
(59, '  sss\r\n', 0, ''),
(60, '  aaaa\r\n', 0, ''),
(61, '  say anything here...', 0, ''),
(62, '  another text here.\r\n', 0, ''),
(63, 'Hi everyone..\r\n', 0, ''),
(64, 'this is another text..\r\n', 0, ''),
(65, 'SSSSSSS\r\n', 0, ''),
(66, '  SSSSSSSSSSSSSSSSSSSSS\r\n', 0, ''),
(67, '  TBGBTRBTR\r\n', 0, ''),
(68, '  dddddcecee\r\n', 0, ''),
(69, '  vwrvwvw\r\n', 0, '');

--
-- 限制导出的表
--

--
-- 限制表 `info`
--
ALTER TABLE `info`
  ADD CONSTRAINT `info_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`user_id`);

--
-- 限制表 `privatemsg`
--
ALTER TABLE `privatemsg`
  ADD CONSTRAINT `privatemsg_ibfk_1` FOREIGN KEY (`privateMsg_Memberid`) REFERENCES `member` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
