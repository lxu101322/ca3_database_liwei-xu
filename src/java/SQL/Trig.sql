use infosystemsql;
DROP TRIGGER IF EXISTS memberTableAction; 
DELIMITER // 
CREATE TRIGGER memberTableAction after INSERT ON member 
FOR EACH ROW 
BEGIN
  INSERT INTO memberTableAction (actions) VALUES ( " new member been generate" );
END // 
DELIMITER ;