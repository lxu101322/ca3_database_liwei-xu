
package Servlet;


import Command.Command;
import Command.CommandFactory;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/UserActionServlet"})
@MultipartConfig
public class UserActionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
       

        public UserActionServlet() {
            super(); 
        }
        
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            processRequest(request, response);
        }
        
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            
        processRequest(request, response);
	}
        
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String forwardToJsp = "";

        
        if ( request.getParameter("action") != null)
        {
          
            CommandFactory factory = CommandFactory.getInstance();
            Command command = factory.createCommand(request.getParameter("action"));
            
            forwardToJsp = command.execute(request, response);
        }
        
       
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardToJsp);
        dispatcher.forward(request, response);
        
    
    }

}
